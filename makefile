all: compile-shaders build-cmake
	build/game

gdb: build-cmake
	gdb -ex run build/game -q

valgrind: build-cmake
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes build/game

build-cmake:
	cmake --build build -j12

clean:
	rm -rf build
	mkdir build
	cmake -G Ninja -S . -B build

documentation:
	doxygen Doxyfile

bear: clean
	bear -- make

compile-shaders:
	glslc shaders/shader.vert -o shaders/vert.spv
	glslc shaders/shader.frag -o shaders/frag.spv
