// #define NDEBUG

#include "Application.h"
#include <cstdlib>

int main(int argc, char **argv) {
  Application app;

  try {
    app.run();
  } catch (const std::exception &e) {
    printf("error: %s", e.what());
    return EXIT_FAILURE;
  }

  return 0;
}
